package logvin.dissertation.scenarios;

import logvin.dissertation.database.FileStorageService;
import logvin.dissertation.forms.tables.CellWithAnchor;
import logvin.dissertation.forms.tables.CellWithRange;
import logvin.dissertation.utils.DissertationException;
import logvin.dissertation.utils.ExcelUtils;
import logvin.dissertation.utils.ParseDirection;
import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Integer.*;
import static java.util.Comparator.comparing;
import static logvin.dissertation.utils.ExceptionModel.*;

/**
 * <b>Слой Excel-сценариев</b>
 *
 * @author logvinIN
 * @since 22.10.2018
 */
public class AllScenarios {

    private ExcelUtils utils = new ExcelUtils();

    private FileStorageService fileStorageService = new FileStorageService();

    /**
     * <b>Сценарий "Суммирование результата" или многократный расчет листа</b>
     * <p>Суммирует значения в переданных ячейках-сумматорах столько раз,
     * сколько указано в ячейке-итераторе</p>
     *
     * @param filePath             путь к рабочей книге
     * @param sheetName            наименование листа рабочей книги
     * @param iteratorCell         адрес ячейки-итератора
     * @param sourceCellsWithRange список целевых ячеек с диапазоном
     * @throws DissertationException если возникли ошибки в сценарии
     */
    public void multiCalculationScenario(String filePath,
                                         String sheetName,
                                         String iteratorCell,
                                         List<CellWithRange> sourceCellsWithRange,
                                         boolean needSumResult,
                                         boolean needMaxValue,
                                         boolean needMinValue,
                                         boolean needAllValues) throws DissertationException {
        utils.fillColumnNames();
        FileInputStream fis;
        Workbook wb;
        try {
            fis = new FileInputStream(filePath);
            wb = WorkbookFactory.create(fis);
        } catch (IOException ex) {
            throw new DissertationException(S_00, filePath);
        }
        fileStorageService.copy(filePath, true);
        Sheet sheet = wb.getSheet(sheetName);
        Row cOmRow = sheet.getRow(utils.getRowIndexByAddress(iteratorCell));
        Cell cOmCell = cOmRow.getCell(utils.getColumnIndexByAddress(iteratorCell));
        double[] sumValues = new double[sourceCellsWithRange.size()];
        Map<String, List<Double>> summsForMaxValue = new HashMap<>();

        for (CellWithRange cell : sourceCellsWithRange) {
            summsForMaxValue.put(cell.getCellAddressValue(), new ArrayList<>());
        }
        Integer value;
        if (utils.isCellParsableToInt(cOmCell)) {
            value = utils.getIntFromDouble(utils.getValueFromCell(cOmCell));
        } else {
            throw new DissertationException(S1_01, cOmCell.getAddress().formatAsString());
        }
        int j = 0;
        if (value != null) {
            if (value != 0) {
                String range;
                String dependentRange;
                String dependentRangeValues;
                int minRange;
                int maxRange;
                int depMinRange;
                int depMaxRange;
                for (int count = 0; count < value; count++) {
                    FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
                    for (CellWithRange cell : sourceCellsWithRange) {
                        //todo check ArrayIndexOutOfBoundsException
                        if (j > sumValues.length - 1) {
                            j = 0; //пробежались по всем ячейкам, бежим заново
                        }
                        range = cell.getRangeValue();
                        minRange = !range.isEmpty() ? parseInt(range.split(";")[0]) : MIN_VALUE;
                        maxRange = !range.isEmpty() ? parseInt(range.split(";")[1]) : MAX_VALUE;
                        if (cell.isDependentCell() && !cell.isOnlyRecalculationInRange()) {
                            dependentRange = cell.getDependentRange();
                            dependentRangeValues = cell.getDepRangeValRange();
                            depMinRange = !dependentRange.isEmpty() ? parseInt(dependentRangeValues.split(";")[0]) : MIN_VALUE;
                            depMaxRange = !dependentRange.isEmpty() ? parseInt(dependentRangeValues.split(";")[1]) : MAX_VALUE;
                            if (!dependentRange.isEmpty()) { //todo падать надо
                                List<String> depCells = utils.parseRangeToCellsDown(dependentRange);
                                for (String depCell : depCells) {
                                    Cell totalDepCell = sheet
                                            .getRow(utils.getRowIndexByAddress(depCell))
                                            .getCell(utils.getColumnIndexByAddress(depCell));
                                    CellValue cv2 = evaluator.evaluate(totalDepCell);
                                    while (cv2.getNumberValue() < depMinRange ||
                                            cv2.getNumberValue() > depMaxRange) {
                                        if (cv2.getNumberValue() == 0.0 ||
                                                Objects.equals(cv2.getStringValue(), "")) {
                                            break;
                                            //пропускаем пустые ячейки
                                        }
                                        double oldValue = cv2.getNumberValue();
                                        evaluator.notifyUpdateCell(totalDepCell);
                                        cv2 = evaluator.evaluate(totalDepCell);
                                        System.out.println((count + 1) + " iteration:\n" +
                                                depCell + ":\n" +
                                                "old value -> " + oldValue + "\n"
                                                + "new value -> " + cv2.getNumberValue());
                                    }
                                }
                                Cell totalCell = sheet
                                        .getRow(utils.getRowIndexByAddress(cell.getCellAddressValue()))
                                        .getCell(utils.getColumnIndexByAddress(cell.getCellAddressValue()));
                                CellValue cv = evaluator.evaluate(totalCell);
                                //суммирование результата
                                sumValues[j] += cv.getNumberValue();
                                //максимальное/минимальное значение
                                summsForMaxValue.get(cell.getCellAddressValue()).add(cv.getNumberValue());
                                j++;
                            }
                        } else if (!cell.isDependentCell() && !cell.isOnlyRecalculationInRange()) {
                            List<String> cellsToOperation = utils.parseRangeToCellsDown(cell.getCellAddressValue());
                            for (String cellToOperation : cellsToOperation) {
                                Cell totalCell = sheet
                                        .getRow(utils.getRowIndexByAddress(cellToOperation))
                                        .getCell(utils.getColumnIndexByAddress(cellToOperation));
                                CellValue cv = evaluator.evaluate(totalCell);
                                while (cv.getNumberValue() < minRange || cv.getNumberValue() > maxRange) {
                                    if (cv.getNumberValue() == 0.0 ||
                                            Objects.equals(cv.getStringValue(), "")) {
                                        break;
                                        //пропускаем пустые ячейки
                                    }
                                    evaluator.notifyUpdateCell(totalCell);
                                    cv = evaluator.evaluate(totalCell);
                                }
                                //суммирование результата
                                sumValues[j] += cv.getNumberValue();
                                //максимальное/минимальное значение
                                summsForMaxValue.get(cell.getCellAddressValue()).add(cv.getNumberValue());
                            }
                            j++;
                        } else if (cell.isOnlyRecalculationInRange() && !cell.isDependentCell()) {
                            List<String> toRecalcCells = utils.parseRangeToCellsDown(cell.getCellAddressValue());
                            for (String cellToRecalc : toRecalcCells) {
                                Cell totalCell = sheet
                                        .getRow(utils.getRowIndexByAddress(cellToRecalc))
                                        .getCell(utils.getColumnIndexByAddress(cellToRecalc));
                                CellValue cv = evaluator.evaluate(totalCell);
                                while (cv.getNumberValue() < minRange || cv.getNumberValue() > maxRange) {
                                    if (cv.getNumberValue() == 0.0 ||
                                            Objects.equals(cv.getStringValue(), "")) {
                                        break;
                                        //пропускаем пустые ячейки
                                    }
                                    evaluator.notifyUpdateCell(totalCell);
                                    cv = evaluator.evaluate(totalCell);
                                }
                            }
                            j++;
                        }
                    }
                }
            }
        } else {
            throw new DissertationException(S1_01, cOmCell.getAddress().formatAsString());
        }
        Sheet resultSheet;
        if (wb.getSheet("Результат") == null) {
            resultSheet = wb.createSheet("Результат");
        } else {
            resultSheet = wb.getSheet("Результат");
            wb.removeSheetAt(wb.getSheetIndex(resultSheet));
            resultSheet = wb.createSheet("Результат");
        }
        int i = 0;
        int lastRowNum;
        for (CellWithRange cell : sourceCellsWithRange) {
            if (!cell.isOnlyRecalculationInRange()) {
                if (needSumResult) {
                    lastRowNum = resultSheet.getLastRowNum();
                    Cell totalSumCellLabel = resultSheet.createRow(lastRowNum + 1).createCell(0);
                    totalSumCellLabel.setCellValue("Общая сумма значений из ячейки " + cell.getCellAddressValue());
                    Cell totalSumCell = resultSheet.getRow(lastRowNum + 1).createCell(1);
                    totalSumCell.setCellValue(sumValues[i]);
                }
                if (needMaxValue) {
                    lastRowNum = resultSheet.getLastRowNum();
                    Cell maxValueLabel = resultSheet.createRow(lastRowNum + 1).createCell(0);
                    Cell maxValue = resultSheet.getRow(lastRowNum + 1).createCell(1);
                    maxValueLabel.setCellValue("Максимальное значение из ячейки " + cell.getCellAddressValue());
                    maxValue.setCellValue(summsForMaxValue
                            .get(cell.getCellAddressValue())
                            .stream()
                            .max(comparing(Double::doubleValue))
                            .get());//todo возможно есть ошибка и надо ее отловить
                }
                if (needMinValue) {
                    lastRowNum = resultSheet.getLastRowNum();
                    Cell minValueLabel = resultSheet.createRow(lastRowNum + 1).createCell(0);
                    Cell minValue = resultSheet.getRow(lastRowNum + 1).createCell(1);
                    minValueLabel.setCellValue("Минимальное значение из ячейки " + cell.getCellAddressValue());
                    minValue.setCellValue(summsForMaxValue
                            .get(cell.getCellAddressValue())
                            .stream()
                            .min(comparing(Double::doubleValue))
                            .get());
                }
                if (needAllValues) {
                    lastRowNum = resultSheet.getLastRowNum();
                    Cell allValuesLabel = resultSheet.createRow(lastRowNum + 1).createCell(0);
                    allValuesLabel.setCellValue("Все значения из ячейки " + cell.getCellAddressValue());
                    for (Double v : summsForMaxValue.get(cell.getCellAddressValue())) {
                        lastRowNum = resultSheet.getLastRowNum();
                        Cell allValues = resultSheet.createRow(lastRowNum + 1).createCell(1);
                        //todo стили
                        allValues.setCellValue(v);
                    }
                }
            }
            i++;
        }
        try {
            closeStreamsAndCreateCopy(filePath, fis, wb);
        } catch (IOException ex) {
            throw new DissertationException(S_01, filePath);
        }
    }

    /**
     * <b>Сценарий "Автозаполнение"</b>
     * <p>Выполняет имитацию автозаполнения (растягивания вниз) ячеек до тех пор, пока в ячейке с условием не будет 1</p>
     *
     * @param filePath       путь к рабочей книге
     * @param sheetName      наименование листа рабочей книги
     * @param rangeList      диапазон ячеек в формате "БукваЧисло:БукваЧисло", либо "БукваЧисло" (например A10:A15 или A10).
     *                       Допускается ввод такого формата: А10, A11:A15, А16 и т.д.
     * @param expressionCell ячейка c условием останова, где в ИСТИНА должно быть 1, а в ЛОЖЬ 0
     * @throws DissertationException если возникли ошибки в сценарии
     */
    public void autoFillingScenario(String filePath,
                                    String sheetName,
                                    List<String> rangeList,
                                    String expressionCell) throws DissertationException {
        //todo валидатор, что формат rangeList верный, т.е. например A10:A15
        utils.fillColumnNames();
        FileInputStream fis;
        Workbook wb;
        try {
            fis = new FileInputStream(filePath);
            wb = WorkbookFactory.create(fis);
        } catch (IOException ex) {
            throw new DissertationException(S_00, filePath);
        }
        fileStorageService.copy(filePath, true);
        wb.setForceFormulaRecalculation(true);
        Sheet sheet = wb.getSheet(sheetName);
        Cell stopCell = sheet
                .getRow(utils.getRowIndexByAddress(expressionCell))
                .getCell(utils.getColumnIndexByAddress(expressionCell));
        FormulaEvaluator ev = wb.getCreationHelper().createFormulaEvaluator();
        int startValue = (int) ev.evaluate(stopCell).getNumberValue();
        int iterationStartValue = 1;
        int copiedStopValue = 1;
        int i = 0;
        List<String> cellsToCopy = new ArrayList<>();
        for (String range : rangeList) {
            if (range.split(":").length < 2) {
                cellsToCopy.add(range);
            } else {
                cellsToCopy.addAll(utils.parseRangeToCellsRight(range));
            }
        }
        while (copiedStopValue != startValue) {
            FormulaEvaluator ev2 = wb.getCreationHelper().createFormulaEvaluator();
            System.out.println(i + " iteration");
            for (String cellToCopy : cellsToCopy) {
                Cell cell;
                if (i == 0) {
                    cell = sheet
                            .getRow(utils.getRowIndexByAddress(cellToCopy))
                            .getCell(utils.getColumnIndexByAddress(cellToCopy));
                } else {
                    cell = sheet
                            .getRow(utils.getRowIndexByAddress(cellToCopy) - 1)
                            .getCell(utils.getColumnIndexByAddress(cellToCopy));
                }
                if (cell != null) {
                    CellStyle cellStyle = cell.getCellStyle();
                    Object cellValue = utils.getValueFromCell(cell);
                    Row row = sheet.getRow(cell.getRowIndex() + 1);
                    if (row == null) {
                        row = sheet.createRow(cell.getRowIndex() + 1);
                    }
                    Cell toFill = row
                            .createCell(cell.getColumnIndex());
                    toFill.setCellFormula(utils.increaseIndexes(String.valueOf(cellValue)));
                    toFill.setCellStyle(cellStyle);
                    ev2
                            .clearAllCachedResultValues();
                }
            }
            ev2.notifyUpdateCell(stopCell);
            startValue = (int) ev2
                    .evaluate(stopCell)
                    .getNumberValue();
            if (copiedStopValue == startValue) {
                startValue = copiedStopValue;
            }
            cellsToCopy = increaseCellsIndexes(cellsToCopy);
            i++;
        }
        try {
            closeStreamsAndCreateCopy(filePath, fis, wb);
        } catch (IOException ex) {
            throw new DissertationException(S_01, filePath);
        }
    }

    /**
     * <b>Сценарий "Замена формул на их значения"</b>
     * <p>Выполняет имитацию специальной вставки значений (в тех же ячейках формулы заменяются на их результат)</p>
     *
     * @param filePath              путь к рабочей книге
     * @param sheetName             наименование листа рабочей книги
     * @param cellsToFormulaReplace диапазон ячеек
     * @throws DissertationException если возникли ошибки в сценарии
     */
    public void copyRandomCellRange(String filePath, String sheetName, List<String> cellsToFormulaReplace) throws DissertationException {
        utils.fillColumnNames();
        FileInputStream fis;
        Workbook wb;
        try {
            fis = new FileInputStream(filePath);
            wb = WorkbookFactory.create(fis);
        } catch (IOException ex) {
            throw new DissertationException(S_00, filePath);
        }
        fileStorageService.copy(filePath, true);

        Sheet sheet = wb.getSheet(sheetName);

        for (String range : cellsToFormulaReplace) {
            for (String cellInRange : utils.parseRangeToCellsDown(range)) {
                Row row = sheet.getRow(utils.getRowIndexByAddress(cellInRange));
                Cell cell = row.getCell(utils.getColumnIndexByAddress(cellInRange));
                CellStyle style = cell.getCellStyle();
                String cellValue = String.valueOf(cell.getNumericCellValue());
                cell.setCellStyle(style);
                cell.setCellFormula(cellValue);
            }
        }
        try {
            closeStreamsAndCreateCopy(filePath, fis, wb);
        } catch (IOException ex) {
            throw new DissertationException(S_01, filePath);
        }
    }


    public void minOrMaxResultWithAnchorRange(String filePath,
                                              String sheetName,
                                              String iteratorCell,
                                              List<CellWithAnchor> cellsWithAnchor,
                                              boolean needMinValue,
                                              boolean needMaxValue) throws DissertationException {
        try{
        utils.fillColumnNames();
        FileInputStream fis;
        Workbook wb;
        try {
            fis = new FileInputStream(filePath);
            wb = WorkbookFactory.create(fis);
        } catch (IOException ex) {
            throw new DissertationException(S_00, filePath);
        }
        fileStorageService.copy(filePath, true);

        Sheet sheet = wb.getSheet(sheetName);
        Cell iterator = sheet
                .getRow(utils.getRowIndexByAddress(iteratorCell))
                .getCell(utils.getColumnIndexByAddress(iteratorCell));
        Map<Double, List<String>> anchorValuesContainer;
        List<Map<String, Map<Double, List<String>>>> forOutput = new ArrayList<>();

        Integer value;
        if (utils.isCellParsableToInt(iterator)) {
            value = utils.getIntFromDouble(utils.getValueFromCell(iterator));
        } else {
            throw new DissertationException(S1_01, iterator.getAddress().formatAsString());
        }
        int j = 0;
        if (value != null) {
            if (value != 0) {
                double[] valuesFromIteration = new double[value];
                for (int count = 0; count < value; count++) {
                    FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
                    for (CellWithAnchor cell : cellsWithAnchor) {
                        boolean toAnchorCellsDown = utils.getRangeParseDirection(cell.getAnchorRange()) == ParseDirection.DOWN;
                        //todo check ArrayIndexOutOfBoundsException
                        if (j > valuesFromIteration.length - 1) {
                            j = 0; //пробежались по всем ячейкам, бежим заново
                        }
                        List<String> anchorCells = toAnchorCellsDown ?
                                utils.parseRangeToCellsDown(cell.getAnchorRange()) :
                                utils.parseRangeToCellsRight(cell.getAnchorRange());
                        Cell targetCell = sheet
                                .getRow(utils.getRowIndexByAddress(cell.getCellAddressValue()))
                                .getCell(utils.getColumnIndexByAddress(cell.getCellAddressValue()));
                        CellValue cv = evaluator.evaluate(targetCell);
                        valuesFromIteration[count] = cv.getNumberValue();
                        List<String> anchorCellsResult = new ArrayList<>();
                        for (String anchorCell : anchorCells) {
                            Cell totalCell = sheet
                                    .getRow(utils.getRowIndexByAddress(anchorCell))
                                    .getCell(utils.getColumnIndexByAddress(anchorCell));
                            anchorCellsResult.add(anchorCell + " = " + evaluator.evaluate(totalCell).getNumberValue());
                        }
                        anchorValuesContainer = new HashMap<>();
                        anchorValuesContainer.put(valuesFromIteration[count],
                                anchorCellsResult
                        );
                        //put в тот же ключ перезаписывает его значения, поэтому заполняем в лист
                        forOutput.add(utils.getHashMap(cell.getCellAddressValue(), anchorValuesContainer));
                        j++;
                    }
                }
            }
        } else {
            throw new DissertationException(S1_01, iterator.getAddress().formatAsString());
        }
        Sheet resultSheet;
        if (wb.getSheet("Результат") == null) {
            resultSheet = wb.createSheet("Результат");
        } else {
            resultSheet = wb.getSheet("Результат");
            wb.removeSheetAt(wb.getSheetIndex(resultSheet));
            resultSheet = wb.createSheet("Результат");
        }
        int i = 0;
        int lastRowNum;
        //todo дублирование убрать
        for (CellWithAnchor cell : cellsWithAnchor) {
            //собрали мапы для конкретной ячейки
            List<Map<String, Map<Double, List<String>>>> mapResult =
                    forOutput
                            .stream()
                            .filter(map -> map.containsKey(cell.getCellAddressValue()))
                            .collect(Collectors.toList());
            if (needMinValue) {
                lastRowNum = resultSheet.getLastRowNum();
                Cell minValueLabel = resultSheet.createRow(lastRowNum + 1).createCell(0);
                Cell minValue = resultSheet.getRow(lastRowNum + 1).createCell(1);
                minValueLabel.setCellValue("Минимальное значение из ячейки " + cell.getCellAddressValue());
                List<String> anchorCells = new ArrayList<>();
                Double minValFromTarget = Double.MAX_VALUE; //дефолтное значение, если оно не изменилось, то это не правильно
                for (Map<String, Map<Double, List<String>>> map : mapResult) {
                    for (Map<Double, List<String>> val : map.values()) {
                        Optional<Double> min = val
                                .keySet()
                                .stream()
                                .findFirst(); //в таком цикле всегда одна мапа, благодаря utils.getHashMap
                        if (min.isPresent() && minValFromTarget > min.get()) {
                            minValFromTarget = min.get();
                            anchorCells = val.get(minValFromTarget);
                        }

                    }
                }
                minValue.setCellValue(minValFromTarget != Double.MAX_VALUE ? Double.toString(minValFromTarget) : null);
                lastRowNum = resultSheet.getLastRowNum();
                Cell anchorLabel = resultSheet.createRow(lastRowNum + 1).createCell(1);
                anchorLabel.setCellValue("Значения из соответствующих ячеек:");
                if (minValFromTarget != Double.MAX_VALUE) {
                    for (String anchorCell : anchorCells) {
                        lastRowNum = resultSheet.getLastRowNum();
                        Cell anchorValue = resultSheet.createRow(lastRowNum + 1).createCell(2);
                        anchorValue.setCellValue(anchorCell);
                    }
                }
            }
            if (needMaxValue) {
                lastRowNum = resultSheet.getLastRowNum();
                Cell maxValueLabel = resultSheet.createRow(lastRowNum + 1).createCell(0);
                Cell maxValue = resultSheet.getRow(lastRowNum + 1).createCell(1);
                maxValueLabel.setCellValue("Максимальное значение из ячейки " + cell.getCellAddressValue());
                List<String> anchorCells = new ArrayList<>();
                Double maxValFromTarget = Double.MIN_VALUE;
                for (Map<String, Map<Double, List<String>>> map : mapResult) {
                    for (Map<Double, List<String>> val : map.values()) {
                        Optional<Double> max = val
                                .keySet()
                                .stream()
                                .findFirst(); //в таком цикле всегда одна мапа, благодаря utils.getHashMap
                        if (max.isPresent() && maxValFromTarget < max.get()) {
                            maxValFromTarget = max.get();
                            anchorCells = val.get(maxValFromTarget);
                        }

                    }
                }
                maxValue.setCellValue(maxValFromTarget != Double.MIN_VALUE ? Double.toString(maxValFromTarget) : null);
                lastRowNum = resultSheet.getLastRowNum();
                Cell anchorLabel = resultSheet.createRow(lastRowNum + 1).createCell(1);
                anchorLabel.setCellValue("Значения из соответствующих ячеек:");
                if (maxValFromTarget != Double.MIN_VALUE) {
                    for (String anchorCell : anchorCells) {
                        lastRowNum = resultSheet.getLastRowNum();
                        Cell anchorValue = resultSheet.createRow(lastRowNum + 1).createCell(2);
                        anchorValue.setCellValue(anchorCell);
                    }
                }
            }
//            if (needAllValues) {
//                lastRowNum = resultSheet.getLastRowNum();
//                Cell allValuesLabel = resultSheet.createRow(lastRowNum + 1).createCell(0);
//                allValuesLabel.setCellValue("Все значения из ячейки " + cell.getCellAddressValue());
//                for (Double v : valuesContainer.get(cell.getCellAddressValue())) {
//                    lastRowNum = resultSheet.getLastRowNum();
//                    Cell allValues = resultSheet.createRow(lastRowNum + 1).createCell(1);
//                    //todo стили
//                    allValues.setCellValue(v);
//                }
//            }
            i++;
        }
        try {
            closeStreamsAndCreateCopy(filePath, fis, wb);
        } catch (IOException ex) {
            throw new DissertationException(S_01, filePath);
        }
        } catch (Exception e) {
            throw new DissertationException(e.getMessage());
        }

    }

    /**
     * <b>Закрывает поток чтения, создает поток записи, записывает в файл все изменения и закрывает поток записи.
     * Затем создает копию файла с признаком {@link logvin.dissertation.database.StorageResult#createdBefore} == false</b>
     *
     * @param filePath путь к рабочей книге
     * @param fis      поток чтения рабочей книги
     * @param wb       рабочая книга
     * @throws IOException           если не удалось закрыть потоки
     * @throws DissertationException если не удалось создать копию рабочей книги
     */
    private void closeStreamsAndCreateCopy(String filePath, FileInputStream fis, Workbook wb) throws IOException, DissertationException {
        FileOutputStream fileOut = new FileOutputStream(filePath);
        wb.write(fileOut);
        fileOut.close();
        fis.close();
        fileStorageService.copy(filePath, false);
    }

    /**
     * <b>Увеличивает индексы ячеек в переданном списке и возвращает новый список</b>
     *
     * @param cells список ячеек для увеличения индексов
     * @return список ячеек с увеличенными на единицу индексами
     */
    private List<String> increaseCellsIndexes(List<String> cells) {
        List<String> increasedCells = new ArrayList<>();
        for (String cell : cells) {
            increasedCells.add(utils.increaseIndexes(cell));
        }

        return increasedCells;
    }
}
