package logvin.dissertation.database;

import logvin.dissertation.utils.DissertationException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static logvin.dissertation.utils.ExceptionModel.*;

/**
 * <b>Сервис работы с БД</b>
 *
 * @author logvinIN
 * @since 17.06.2019
 */
public class DatabaseService {

    private static final String FILE_STORAGE_URL = "jdbc:sqlite::resource:resources/dissertation.db";

    /**
     * <b>Устанавливает соединение с базой данных</b>
     *
     * @return Интерфейс соединения. Может быть null
     * @throws DissertationException если соединение не удалось установить
     */
    private Connection connect() throws DissertationException {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(FILE_STORAGE_URL);
        } catch (SQLException e) {
            throw new DissertationException(BD_01);
        }
        return conn;
    }

    /**
     * <b>Закрывает объект интерфейса соединения с БД</b>
     *
     * @param object Объект соединения
     * @throws DissertationException если не удалось закрыть объект соединения
     */
    private <T extends AutoCloseable> void closeDatabaseObject(T object) throws DissertationException {
        if (object != null) {
            try {
                object.close();
            } catch (Exception e) {
                throw new DissertationException(BD_02, object.toString());
            }
        }
    }

    /**
     * <b>Создает таблицу хранения атрибутов копии, если она не существует в БД</b>
     *
     * @throws DissertationException если не удалось создать
     */
    public void createIfNotExists() throws DissertationException {
        String query = "CREATE TABLE IF NOT EXISTS FILE_STORAGE (" +
                "ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ," +
                "GUID              VARCHAR NOT NULL," +
                "CREATE_DATE       VARCHAR NOT NULL," +
                "NAME              VARCHAR NOT NULL," +
                "EXPANSION         VARCHAR NOT NULL," +
                "IS_CREATED_BEFORE BOOLEAN NOT NULL)";
        Connection conn = connect();
        try {
            Statement pstmt = conn.createStatement();
            pstmt.executeUpdate(query);
        } catch (SQLException e) {
            throw new DissertationException(BD_03);
        }
        closeDatabaseObject(conn);
    }

    /**
     * <b>Добавляет запись в таблицу FILE_STORAGE</b>
     *
     * @param guid         гуид файла
     * @param createDate   дата создания файла (строка)
     * @param name         наименование файла (без расширения)
     * @param expansion    расширение файла
     * @param isCopyBefore признак того, что копия создана "до" прогона сценария
     */
    public void insert(String guid, String createDate, String name, String expansion, boolean isCopyBefore) throws DissertationException {
        String sql = "INSERT INTO FILE_STORAGE (GUID, CREATE_DATE, NAME, EXPANSION, IS_CREATED_BEFORE) " +
                "VALUES(?,?,?,?,?)";
        Connection conn = connect();
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, guid);
            pstmt.setString(2, createDate);
            pstmt.setString(3, name);
            pstmt.setString(4, expansion);
            pstmt.setBoolean(5, isCopyBefore);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DissertationException(BD_04);
        }
        closeDatabaseObject(conn);
    }

    /**
     * <b>Удаляет запись из таблицы по гуиду</b>
     *
     * @param guid Гуид записи
     * @throws DissertationException если не удалось выполить запрос
     */
    public void delete(String guid) throws DissertationException {
        String sql = "DELETE FROM FILE_STORAGE WHERE GUID = '" + guid + "'";
        List<StorageResult> resultByGuid = selectByGuid(guid);
        Connection conn = connect();
        try {
            if (resultByGuid.size() > 0) {
                Statement pstmt = conn.createStatement();
                pstmt.execute(sql);
            }
        } catch (SQLException e) {
            throw new DissertationException(BD_05);
        }
        closeDatabaseObject(conn);
    }

    /**
     * <b>Удаляет все записи из таблицы</b>
     *
     * @return признак успешного удаления
     * @throws DissertationException если не удалось выполнить запрос
     */
    public boolean deleteAll() throws DissertationException {
        String sql = "DELETE FROM FILE_STORAGE WHERE GUID NOT NULL";
        Connection conn = connect();
        try {
            Statement pstmt = conn.createStatement();
            pstmt.execute(sql);
        } catch (SQLException e) {
            throw new DissertationException(BD_06);
        }
        closeDatabaseObject(conn);
        return true;
    }

    /**
     * <b>Выгружает все записи из таблицы</b>
     *
     * @return список {@link StorageResult}
     */
    public List<StorageResult> selectAll() throws DissertationException {
        String sql = "SELECT * FROM FILE_STORAGE";
        return select(sql);
    }

    /**
     * <b>Выгружает запись по гуиду</b>
     *
     * @param guid гуид записи
     * @return список {@link StorageResult} (size() == 0, либо size == 1)
     */
    private List<StorageResult> selectByGuid(String guid) throws DissertationException {
        String sql = "SELECT * FROM FILE_STORAGE WHERE GUID = '" + guid + "'";
        return select(sql);
    }

    /**
     * <b>Выгружает записи из таблицы по определенному запросу</b>
     *
     * @param query запрос для выгрузки записей
     * @return список {@link StorageResult}
     */
    private List<StorageResult> select(String query) throws DissertationException {
        List<StorageResult> convertedResult = new ArrayList<>();
        Connection connection = connect();
        try {
            Statement pstmt = connection.createStatement();
            ResultSet result = pstmt.executeQuery(query);
            while (result.next()) {
                convertedResult.add(new StorageResult(
                        result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5),
                        result.getBoolean(6)
                ));
            }
        } catch (SQLException e) {
            throw new DissertationException(BD_07);
        }
        closeDatabaseObject(connection);
        return convertedResult;
    }
}
