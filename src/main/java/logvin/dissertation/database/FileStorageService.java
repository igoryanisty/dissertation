package logvin.dissertation.database;

import logvin.dissertation.utils.DissertationException;
import org.apache.commons.io.FileUtils;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static logvin.dissertation.utils.ExceptionModel.*;

/**
 * <b>Промежуточный слой работы с БД, а также основной слой работы с файловой системой компьютера</b>
 * <p>В данном классе подготавливаются входные данные для передачи в сервис работы с БД</p>
 *
 * @author logvinIN
 * @since 16.06.2019
 */
public class FileStorageService {

    /**
     * Путь до директории, где будут храниться копии документов
     */
    private static final String PATH_TO_STORAGE_DIR = System.getProperty("user.home") + "/excel-scenario/storage";

    /**
     * Объект сервиса работы с БД
     */
    private DatabaseService databaseService = new DatabaseService();

    /**
     * <b>Копирует файл из переданного пути в директорию файлового хранилища в системе пользователя</b>
     *
     * @param pathToSource путь до файла, чью копию необходимо сделать
     * @param isBefore     признак того, что копия создается "до" прогона сценария
     * @throws DissertationException если не удалось скопировать файл
     */
    public void copy(String pathToSource, boolean isBefore) throws DissertationException {
        createDirIfNotExists(FileUtils.getUserDirectoryPath() + "/excel-scenario", false);
        createDirIfNotExists(PATH_TO_STORAGE_DIR, true);
        String formattedDate = getFileCreateDate();
        File source = new File(pathToSource);
        FileProperties fileProperties = new FileProperties().getPropertiesFromPath(pathToSource);
        UUID guid = UUID.randomUUID();
        String nameOfCopyToDir = guid.toString() + "." + fileProperties.getExpansion();
        File dest = new File(PATH_TO_STORAGE_DIR + "/" + nameOfCopyToDir);
        try {
            FileUtils.copyFile(source, dest);
            hideFile(dest);
            databaseService.insert(guid.toString(), formattedDate, fileProperties.getName(), fileProperties.getExpansion(), isBefore);
        } catch (IOException e) {
            throw new DissertationException(FS_01, PATH_TO_STORAGE_DIR);
        }
    }

    /**
     * <b>Обращается к сервису работы с БД для создания таблицы хранения атрибутов копии</b>
     *
     * @throws DissertationException если не удалось создать таблицу
     */
    public void createTableIfNotExist() throws DissertationException {
        databaseService.createIfNotExists();
    }

    /**
     * <b>Открывает файл с переданным именем</b>
     * <p>Если передан признак fileInStorage == true, то в fileName надо передавать только наименования файла с расширением</p>
     * <p>Если признак fileInStorage == false, то надо передавать полный путь к файлу</p>
     *
     * @param fileName      путь к файлу с расширением
     * @param fileInStorage признак нахождения файла в директории файлового хранилища
     * @throws DissertationException если не удалось открыть файл
     */
    public void openFile(String fileName, boolean fileInStorage) throws DissertationException {
        File file;
        file = fileInStorage ? new File(PATH_TO_STORAGE_DIR + "/" + fileName) : new File(fileName);
        if (!Desktop.isDesktopSupported()) throw new DissertationException(FS_03);

        Desktop desktop = Desktop.getDesktop();
        if (file.exists()) {
            try {
                desktop.open(file);
            } catch (IOException ex) {
                throw new DissertationException(FS_04, file.getAbsolutePath());
            }
        }
    }

    /**
     * <b>Удаляет файл из директории файлового хранилища по наименованию файла</b>
     * <p>Сначала удаляет запись о файле из БД</p>
     *
     * @param guidWithExpansion гуид файла + . + расширение файла
     * @throws DissertationException если не удалось удалить файл
     */
    public void deleteFile(String guidWithExpansion) throws DissertationException {
        File file;
        file = new File(PATH_TO_STORAGE_DIR + "/" + guidWithExpansion);
        if (file.exists()) {
            try {
                FileUtils.forceDelete(file);
                databaseService.delete(guidWithExpansion.split(".")[0]);
            } catch (IOException ex) {
                throw new DissertationException(FS_05, file.getAbsolutePath());
            }
        }
    }

    /**
     * <b>Удаляет все файлы из директории и из базы</b>
     *
     * @throws DissertationException если не удалось удалить файлы
     */
    public void deleteAllFiles() throws DissertationException {
        File file;
        file = new File(PATH_TO_STORAGE_DIR);
        if (file.exists()) {
            try {
                FileUtils.cleanDirectory(file);
                databaseService.deleteAll();
            } catch (IOException ex) {
                throw new DissertationException(FS_06, file.getAbsolutePath());
            }
        }
    }

    /**
     * <b>Обращается к сервису работы с БД для выгрузки всех записей</b>
     *
     * @return список {@link StorageResult}
     * @throws DissertationException если не удалось выгрузить файлы
     */
    public List<StorageResult> getAllFiles() throws DissertationException {
        return databaseService.selectAll();
    }

    /**
     * <b>Создает директорию по переданному пути и скрывает ее, если необходимо</b>
     *
     * @param pathToDir путь до места, где должна быть создана директория (имя директории включительно)
     * @param hideDir   признак необходимости сокрытия директории
     * @throws DissertationException если не удалось задать скрытый атрибут директории
     */
    private void createDirIfNotExists(String pathToDir, boolean hideDir) throws DissertationException {
        File theDir = new File(pathToDir);
        if (!theDir.exists()) {
            theDir.mkdir();
            if (hideDir) {
                hideFile(theDir);
            }
        }
    }

    /**
     * <b>Скрывает директорию/файл</b>
     *
     * @param file файл для сокрытия
     * @throws DissertationException если не удалось задать скрытое свойство файлу
     */
    private void hideFile(File file) throws DissertationException {
        try {
            Files.setAttribute(file.toPath(), "dos:hidden", true, LinkOption.NOFOLLOW_LINKS);
        } catch (IOException se) {
            throw new DissertationException(FS_02, file.getAbsolutePath());
        }
    }

    /**
     * <b>Получить дату создания файла в часовом поясе пользователя</b>
     * <p>В базу кладется именно это значение, т.к. есть проблемы, что SQLite не дает задать свой часовой пояс</p>
     *
     * @return дата в формате dd.MM.yyyy HH:mm:ss
     */
    private String getFileCreateDate() {
        Long dateInTime = System.currentTimeMillis();
        Instant instant = Instant.ofEpochMilli(dateInTime);
        LocalDateTime localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        return localDate.format(formatter);
    }
}
