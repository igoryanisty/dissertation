package logvin.dissertation.database;

/**
 * <b>Модель идентификационных атрибутов файла</b>
 *
 * @author logvinIN
 * @since 23.06.2019
 */
public class FileProperties {

    /**
     * Наименование
     */
    private String name;

    /**
     * Расширение
     */
    private String expansion;

    FileProperties() {
    }

    public String getName() {
        return name;
    }

    public String getExpansion() {
        return expansion;
    }

    /**
     * <b>Заполняет атрибуты модели, опираясь на переданный путь</b>
     *
     * @param pathToSource путь до файла
     * @return модель с атрибутами файла
     */
    public FileProperties getPropertiesFromPath(String pathToSource) {
        String[] path = pathToSource.split("[\\\\]");
        String fileNameWithExpansion = path[path.length - 1];
        String[] fileName = fileNameWithExpansion.split("\\.");
        this.name = fileName[0];
        this.expansion = fileName[1];
        return this;
    }
}
