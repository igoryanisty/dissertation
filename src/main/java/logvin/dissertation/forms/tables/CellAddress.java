package logvin.dissertation.forms.tables;

import javafx.beans.property.StringProperty;

/**
 * <b>Модель JavaFX таблицы для хранения адресов и диапазонов ячеек</b>
 *
 * @author logvinIN
 * @since 07.03.2019
 */
public class CellAddress {

    /**
     * Строковой атрибут колонки таблицы
     */
    private StringProperty cellAddressProperty;

    public CellAddress(StringProperty cellAddressProperty) {
        this.cellAddressProperty = cellAddressProperty;
    }

    public String getCellAddressValue() {
        return cellAddressProperty.get();
    }

    public StringProperty cellAddressProperty() {
        return cellAddressProperty;
    }
}
