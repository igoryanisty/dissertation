package logvin.dissertation.forms.tables;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

/**
 * <b>Модель таблицы файлового хранилища</b>
 *
 * @author logvinIN
 * @since 22.06.2019
 */
public class FileInStorage {

    /**
     * Целочисленный атрибут порядкового номера записи
     */
    private IntegerProperty number;

    /**
     * Строковой атрибут даты создания
     */
    private StringProperty createDate;

    /**
     * Строковой атрибут наименования
     */
    private StringProperty name;

    /**
     * Строковой атрибут гуида
     */
    private StringProperty guid;

    /**
     * Строковой атрибут расширения
     */
    private StringProperty expansion;

    /**
     * Логический атрибут признака того, что копия создана "до"
     */
    private BooleanProperty createdBefore;

    public FileInStorage(IntegerProperty number,
                         StringProperty createDate,
                         StringProperty name,
                         StringProperty guid,
                         StringProperty expansion,
                         BooleanProperty createdBefore) {
        this.number = number;
        this.createDate = createDate;
        this.name = name;
        this.guid = guid;
        this.expansion = expansion;
        this.createdBefore = createdBefore;
    }

    public Integer getNumber() {
        return number.get();
    }

    public void setNumber(int number) {
        this.number.set(number);
    }

    public IntegerProperty numberProperty() {
        return number;
    }

    public String getCreateDate() {
        return createDate.get();
    }

    public void setCreateDate(String createDate) {
        this.createDate.set(createDate);
    }

    public StringProperty createDateProperty() {
        return createDate;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getGuid() {
        return guid.get();
    }

    public void setGuid(String guid) {
        this.guid.set(guid);
    }

    public StringProperty guidProperty() {
        return guid;
    }

    public String getExpansion() {
        return expansion.get();
    }

    public void setExpansion(String expansion) {
        this.expansion.set(expansion);
    }

    public StringProperty expansionProperty() {
        return expansion;
    }

    public boolean isCreatedBefore() {
        return createdBefore.get();
    }

    public void setCreatedBefore(boolean createdBefore) {
        this.createdBefore.set(createdBefore);
    }

    public BooleanProperty createdBeforeProperty() {
        return createdBefore;
    }

    /**
     * <b>Конкатенирует гуид и расширение</b>
     *
     * @return {@link #getGuid()} + "." + {@link #getExpansion()}
     */
    public String concatFileNameByGuid() {
        return getGuid() + "." + getExpansion();
    }

    /**
     * <b>Конкатенирует наименование и расширение</b>
     *
     * @return {@link #getName()} + "." + {@link #getExpansion()}
     */
    public String concatFileNameByName() {
        return getName() + "." + getExpansion();
    }
}
