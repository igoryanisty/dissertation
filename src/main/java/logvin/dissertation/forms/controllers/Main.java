package logvin.dissertation.forms.controllers;

import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import logvin.dissertation.database.FileStorageService;
import logvin.dissertation.forms.utils.ControllerUtils;
import logvin.dissertation.utils.DissertationException;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * <b>Контроллер главной формы</b>
 *
 * @author logvinIN
 * @since 23.06.2019
 */
public class Main implements Initializable {

    /**
     * Поле ввода пути к файлу
     */
    public TextField path_textField;

    /**
     * Кнопка "Указать путь к документу"
     */
    public Button chooseFile_button;

    /**
     * Перключатель "Итерационные вычисления"
     */
    public RadioButton iterationCalculation_radio;

    /**
     * Переключатель "Автозаполнение"
     */
    public RadioButton autofilling_radio;

    /**
     * Переключатель "Замена формул на значение"
     */
    public RadioButton formulaReplace_radio;

    /**
     * Кнопка "Запуск"
     */
    public Button run_button;

    /**
     * Кнопка "Открыть файловое хранилище
     */
    public Button openFileStorage_button;

    /**
     * Решение задачи оптимизации
     */
    public RadioButton optimizationProblem_radio;

    private ControllerUtils utils = new ControllerUtils();

    private FileStorageService fileStorage = new FileStorageService();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        utils.setDisableOnButton(true, run_button);
        path_textField.textProperty().addListener(event -> checkIsAllFieldsValid());
    }

    /**
     * <b>Открывает системное окно выбора файла</b>
     */
    public void chooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл");
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            path_textField.clear();
            path_textField.appendText(file.getAbsolutePath());
            checkIsAllFieldsValid();
        }
    }

    /**
     * <b>Обрабатывает действие при выборе переключателя "Суммирование результата"</b>
     */
    //todo убрать дублирование
    public void onIterationCalculationRadio() {
        iterationCalculation_radio.setSelected(true);
        autofilling_radio.setSelected(false);
        formulaReplace_radio.setSelected(false);
        optimizationProblem_radio.setSelected(false);
        checkIsAllFieldsValid();
    }

    /**
     * <b>Обрабатывает действие при выборе переключателя "Автозаполнение"</b>
     */
    public void onAutoFillingRadio() {
        autofilling_radio.setSelected(true);
        iterationCalculation_radio.setSelected(false);
        formulaReplace_radio.setSelected(false);
        optimizationProblem_radio.setSelected(false);
        checkIsAllFieldsValid();
    }

    /**
     * <b>Обрабатывает действие при выборе переключателя "Замена формул на значение"</b>
     */
    public void onFormulaReplaceRadio() {
        formulaReplace_radio.setSelected(true);
        iterationCalculation_radio.setSelected(false);
        autofilling_radio.setSelected(false);
        optimizationProblem_radio.setSelected(false);
        checkIsAllFieldsValid();
    }

    /**
     * <b>Обрабатывает действие при выборе переключателя "Замена формул на значение"</b>
     */
    public void onOptimizationProblemRadio() {
        optimizationProblem_radio.setSelected(true);
        formulaReplace_radio.setSelected(false);
        iterationCalculation_radio.setSelected(false);
        autofilling_radio.setSelected(false);
        checkIsAllFieldsValid();
    }

    /**
     * <b>Запускает выбранный сценарий</b>
     * <p>Дизейблит все кнопки, пока открыто любое окно сценария</p>
     */
    public void run() {
        //todo контроли на заполнение всех полей
        //todo наименования форм в константы
        try {
            RadioButton selectedRadio = getSelectedRadio();
            fileStorage.createTableIfNotExist();
            FxmlLoaderModel loader;
            Stage scenarioStage = new Stage();
            if (selectedRadio == iterationCalculation_radio) {
                loader = utils.loadFxmlModel(utils.getLoaderWithSource("IterationCalculation.fxml"));
                IterationCalculation controller = loader.getLoader().getController();
                controller.setFilePath(path_textField.getText());
                scenarioStage = utils.showForm("Итерационные вычисления", loader.getParent());
                if (scenarioStage.isShowing()) {
                    disableButtons(true);
                }
            } else if (selectedRadio == autofilling_radio) {
                loader = utils.loadFxmlModel(utils.getLoaderWithSource("AutoFilling.fxml"));
                AutoFilling controller = loader.getLoader().getController();
                controller.setFilePath(path_textField.getText());
                scenarioStage = utils.showForm("Автозаполнение", loader.getParent());
                if (scenarioStage.isShowing()) {
                    disableButtons(true);
                }
            } else if (selectedRadio == formulaReplace_radio) {
                loader = utils.loadFxmlModel(utils.getLoaderWithSource("FormulaReplacing.fxml"));
                FormulaReplacing controller = loader.getLoader().getController();
                controller.setFilePath(path_textField.getText());
                scenarioStage = utils.showForm("Замена формул на значение", loader.getParent());
                if (scenarioStage.isShowing()) {
                    disableButtons(true);
                }
            } else if (selectedRadio == optimizationProblem_radio) {
                loader = utils.loadFxmlModel(utils.getLoaderWithSource("OptimizationProblem.fxml"));
                OptimizationProblem controller = loader.getLoader().getController();
                controller.setFilePath(path_textField.getText());
                scenarioStage = utils.showForm("Решение задачи оптимизации", loader.getParent());
                if (scenarioStage.isShowing()) {
                    disableButtons(true);
                }
            }
            scenarioStage.setOnCloseRequest(request -> disableButtons(false));
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка предварительных алгоритмов",
                    ex.getMessage());
        }
    }

    /**
     * <b>Открывает окно "Файловое хранилище"</b>
     */
    public void openFileStorage() {
        try {
            utils.showForm("Файловое хранилище",
                    utils.loadFxmlModel(utils.getLoaderWithSource("FileStorage.fxml")).getParent());
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка открытия файлового хранилища",
                    ex.getMessage());
        }
    }


    /**
     * <b>Возвращает выбранный пользователем переключатель</b>
     *
     * @return {@link #iterationCalculation_radio}, либо {@link #autofilling_radio}, либо {@link #formulaReplace_radio}
     */
    private RadioButton getSelectedRadio() {
        if (iterationCalculation_radio.isSelected()) {
            return iterationCalculation_radio;
        } else if (optimizationProblem_radio.isSelected()) {
            return optimizationProblem_radio;
        } else if (autofilling_radio.isSelected()) {
            return autofilling_radio;
        } else if (formulaReplace_radio.isSelected()) {
            return formulaReplace_radio;
        } else {
            return null;
        }
    }

    /**
     * <b>Не дает, либо дает, пользователю нажимать на кнопки</b>
     *
     * @param disabled признак того, что надо задизейблить кнопки
     */
    private void disableButtons(boolean disabled) {
        utils.setDisableOnButton(disabled,
                chooseFile_button,
                iterationCalculation_radio,
                autofilling_radio,
                formulaReplace_radio,
                run_button);
    }

    /**
     * <b>Проверяет, что все входные данные для запуска сценария заполнены</b>
     */
    private void checkIsAllFieldsValid() {
        if (path_textField.getText().isEmpty() || getSelectedRadio() == null) {
            utils.setDisableOnButton(true, run_button);
        } else {
            utils.setDisableOnButton(false, run_button);
        }
    }
}
