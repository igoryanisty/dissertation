package logvin.dissertation.forms.controllers;

import javafx.collections.ObservableList;

/**
 * <b>Интерфейс контроллеров JavaFX форм, запускающих сценарии</b>
 *
 * @author logvinIN
 * @since 30.06.2019
 */
public interface DissertationController {

    /**
     * Получить данные для таблицы
     */
    ObservableList getTableData();

    /**
     * Добавить в таблицу
     */
    void addInTable();


    /**
     * Удалить одну запись из таблицы
     */
    void deleteItem();

    /**
     * Очистить таблицу
     */
    void clearTable();

    /**
     * Очистить все элементы формы
     */
    void clearAll();

    /**
     * Запустить сценарий
     */
    void runScenario();
}
