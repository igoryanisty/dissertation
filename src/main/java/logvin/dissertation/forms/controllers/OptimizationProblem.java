package logvin.dissertation.forms.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import logvin.dissertation.forms.tables.CellWithAnchor;
import logvin.dissertation.forms.utils.ControllerUtils;
import logvin.dissertation.scenarios.AllScenarios;
import logvin.dissertation.utils.DissertationException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * @author logvinIN
 * @since 10.09.2019
 */
public class OptimizationProblem implements DissertationController, Initializable {


    public TableView<CellWithAnchor> table;
    public TableColumn<CellWithAnchor, String> targetCell_column;
    public TableColumn<CellWithAnchor, String> anchorRange_column;
    public MenuItem delete_menuItem;
    public TextField targetCell_textField;
    public TextField iteratorCell_textField;
    public TextField sheetName_textField;
    public TextField anchorRange_textField;
    public Button clearTable_button;
    public Button clearAll_button;
    public Button ok_button;
    public CheckBox min_checkbox;
    public CheckBox max_checkbox;
    public Button addRow_button;

    private ControllerUtils utils = new ControllerUtils();

    private String filePath;

    @Override
    public ObservableList<CellWithAnchor> getTableData() {
        return FXCollections.observableArrayList();
    }

    @Override
    public void addInTable() {
        table.getItems().add(new CellWithAnchor(
                new SimpleStringProperty(targetCell_textField.getText()),
                new SimpleStringProperty(anchorRange_textField.getText())
        ));
        targetCell_textField.clear();
        anchorRange_textField.clear();
    }

    @Override
    public void deleteItem() {
        CellWithAnchor selectedItem = table.getSelectionModel().getSelectedItem();
        table.getItems().remove(selectedItem);
    }

    @Override
    public void clearTable() {
        table.getItems().clear();
    }

    @Override
    public void clearAll() {
        targetCell_textField.clear();
        anchorRange_textField.clear();
        sheetName_textField.clear();
        iteratorCell_textField.clear();
    }

    @Override
    public void runScenario() {
        List<CellWithAnchor> sourceCells = new ArrayList<>();
        for (int i = 0; i < table.getItems().size(); i++) {
            sourceCells = table.getItems()
                    .stream()
                    .map(cell -> new CellWithAnchor(
                            cell.getCellAddressValue(),
                            cell.getAnchorRange()
                    ))
                    .collect(Collectors.toList());
        }
        try {
            new AllScenarios().minOrMaxResultWithAnchorRange(
                    filePath,
                    sheetName_textField.getText(),
                    iteratorCell_textField.getText(),
                    sourceCells,
                    min_checkbox.isSelected(),
                    max_checkbox.isSelected()
            );
            utils.showEndScenarioWindow(filePath);
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка сценария \"Решение оптимизационной задачи\"",
                    ex.getMessage());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        targetCell_column.setCellValueFactory(cellData -> cellData.getValue().cellAddressProperty());
        anchorRange_column.setCellValueFactory(cellData -> cellData.getValue().anchorRangeProperty());
        table.setItems(getTableData());
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
