package logvin.dissertation.forms.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

/**
 * <b>Вспомогательная модель для инициализации JavaFX форм</b>
 *
 * @author logvinIN
 * @since 30.06.2019
 */
public class FxmlLoaderModel {

    /**
     * Текущий лоадер
     */
    private FXMLLoader loader;

    /**
     * Текущий родитель формы
     */
    private Parent parent;

    public FxmlLoaderModel(FXMLLoader loader) throws IOException {
        this.loader = loader;
        this.parent = this.loader.load();
    }

    public FXMLLoader getLoader() {
        return loader;
    }

    public Parent getParent() {
        return parent;
    }
}
