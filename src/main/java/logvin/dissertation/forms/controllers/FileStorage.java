package logvin.dissertation.forms.controllers;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import logvin.dissertation.database.FileStorageService;
import logvin.dissertation.database.StorageResult;
import logvin.dissertation.forms.tables.FileInStorage;
import logvin.dissertation.forms.utils.ControllerUtils;
import logvin.dissertation.utils.DissertationException;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * <b>Контроллер формы "Файловое хранилище"</b>
 *
 * @author logvinIN
 * @since 22.06.2019
 */
public class FileStorage implements Initializable {

    /**
     * Таблица с атрибутами {@link FileInStorage}
     */
    public TableView<FileInStorage> storage_table;

    /**
     * Конекстное меню для таблицы {@link #storage_table}
     */
    public ContextMenu contextMenu_column;

    /**
     * Колонка таблицы {@link #storage_table} "№"
     */
    public TableColumn<FileInStorage, Integer> number_column;

    /**
     * Колонка таблицы {@link #storage_table} "Дата создания"
     */
    public TableColumn<FileInStorage, String> createDate_column;

    /**
     * Колонка таблицы {@link #storage_table} "Наименование"
     */
    public TableColumn<FileInStorage, String> name_column;

    /**
     * Скрытая колонка таблицы {@link #storage_table} "_guid"
     */
    public TableColumn guid_column;

    /**
     * Скрытая колонка таблицы {@link #storage_table} "_expansion"
     */
    public TableColumn expansion_column;

    /**
     * Скрытая колонка таблицы {@link #storage_table} "isCopyBefore"
     */
    public TableColumn isCopyBefore_column;

    /**
     * Пункт контекстного меню "Открыть"
     */
    public MenuItem open_action;

    /**
     * Пункт контекстного меню "Удалить"
     */
    public MenuItem delete_item;

    /**
     * Поле для поиска по имени файла
     */
    public TextField search_field;

    /**
     * Кнопка "Найти"
     */
    public Button search_button;

    /**
     * Надпись к таблице {@link #storage_table}
     */
    public Label table_label;

    /**
     * Кнопка "Очистить поле поиска"
     */
    public Button clearSearchField_button;

    /**
     * Кнопка "Сбросить фильтр"
     */
    public Button clearFilter_button;

    /**
     * Кнопка "Удалить все файлы"
     */
    public Button deleteAll_button;

    /**
     * Флажок "Только копии "до""
     */
    public CheckBox onlyAfterCopies_checkbox;

    /**
     * Флажок "Только копии "после""
     */
    public CheckBox onlyBeforeCopies_checkbox;

    /**
     * Список моделей {@link FileInStorage}
     */
    private ObservableList<FileInStorage> loadedFiles;

    private ControllerUtils utils = new ControllerUtils();

    private FileStorageService fileStorage = new FileStorageService();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        table_label.setText("Таблица с атрибутами созданных копий.\n" +
                "Нажмите правой кнопкой мыши на нужный файл \n" +
                "и выберите необходимую операцию.\n" +
                "Вы так же можете ввести текст в поле для поиска.\n" +
                "Система будет проводить поиск по колонке \"Наименование\"\n" +
                "Для поиска введите текст в поле и нажмите \"Найти\"\n" +
                "Чтобы сбросить результаты поиска, нажмите \"Сбросить фильтр\".");
        number_column.setCellValueFactory(cellData -> cellData.getValue().numberProperty().asObject());
        createDate_column.setCellValueFactory(cellData -> cellData.getValue().createDateProperty());
        name_column.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        loadedFiles = getTableData();
        storage_table.getItems().addAll(loadedFiles);
        storage_table.setContextMenu(contextMenu_column);
    }

    /**
     * <b>Удаляет выделенный элемент таблицы</b>
     */
    public void deleteRow() {
        FileInStorage selectedItem = storage_table.getSelectionModel().getSelectedItem();
        try {
            fileStorage.deleteFile(selectedItem.concatFileNameByGuid());
            loadedFiles.remove(selectedItem);
            rewriteNumbers();
            storage_table.getItems().clear();
            storage_table.getItems().addAll(loadedFiles);
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка удаления файла " + selectedItem.concatFileNameByName(),
                    ex.getMessage());
        }
    }

    /**
     * <b>Удаляет все файлы из фалового хранилища и из БД, если пользователь согласен с этим</b>
     */
    public void deleteAll() {
        boolean acceptDelete = utils.getConfirmResult("Удаление всех файлов",
                "Вы уверены, что хотите удалить все файлы?\n" +
                        "Восстановить их будет невозможно.");
        if (acceptDelete) {
            try {
                fileStorage.deleteAllFiles();
                loadedFiles.clear();
                storage_table.getItems().clear();
            } catch (DissertationException ex) {
                utils.showAlertWindow(Alert.AlertType.ERROR, "Ошибка удаления всех файлов",
                        ex.getMessage());
            }
        }
    }

    /**
     * <b>Фильтрует записи по признаку "только копии "до""</b>
     */
    public void filterByBeforeProperty() {
        if (!onlyAfterCopies_checkbox.isSelected()) {
            filterByCheckbox(onlyBeforeCopies_checkbox, true);
        }
        if (onlyAfterCopies_checkbox.isSelected()) {
            filterByCheckbox(onlyAfterCopies_checkbox, false);
        }
    }

    /**
     * <b>Фильтрует записи по признаку "только копии "после""</b>
     */
    public void filterByAfterProperty() {
        if (!onlyBeforeCopies_checkbox.isSelected()) {
            filterByCheckbox(onlyAfterCopies_checkbox, false);
        }
        if (onlyBeforeCopies_checkbox.isSelected()) {
            filterByCheckbox(onlyBeforeCopies_checkbox, true);
        }
    }

    /**
     * <b>Фильтрует записи по выбранному чекбоксу</b>
     *
     * @param checkBox     флажок либо {@link #onlyBeforeCopies_checkbox}, либо {@link #onlyAfterCopies_checkbox}
     * @param isBeforeCopy признак того, что надо фильтровать по "до"
     */
    private void filterByCheckbox(CheckBox checkBox, boolean isBeforeCopy) {
        storage_table.getItems().clear();
        if (checkBox.isSelected()) {
            storage_table.getItems().addAll(loadedFiles
                    .stream()
                    .filter(file -> file.isCreatedBefore() == isBeforeCopy)
                    .collect(Collectors.toList()));
        } else {
            storage_table.getItems().addAll(loadedFiles);
        }
    }

    /**
     * <b>Открывает файл по наименованию из выбранной в таблице записи</b>
     * <p>наименование берется из скрытой колонки {@link #guid_column}</p>
     */
    public void openFile() {
        FileInStorage selectedItem = storage_table.getSelectionModel().getSelectedItem();
        String fileName = selectedItem.getGuid() + "." + selectedItem.getExpansion();
        utils.showAlertWindow(Alert.AlertType.INFORMATION,
                "Операция открытия документа (окно можно закрыть)",
                "Запущен процесс открытия документа с именем\n" + fileName + "");
        try {
            fileStorage.openFile(fileName, true);
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR,
                    "Ошибка открытия файла",
                    ex.getMessage());
        }
    }

    /**
     * <b>Очищает поле ввода имени файла</b>
     */
    public void clearSearchField() {
        search_field.clear();
    }

    /**
     * <b>Сбрасывает результаты поиска и чекбоксы</b>
     */
    public void clearFilteredTable() {
        storage_table.getItems().clear();
        storage_table.getItems().addAll(loadedFiles);
        onlyAfterCopies_checkbox.setSelected(false);
        onlyBeforeCopies_checkbox.setSelected(false);
    }

    /**
     * <b>Ищет в выборке записей из БД ({@link #getTableData()}) запись по частичному вхождению в строку</b>
     */
    public void findFileByName() {
        String fileNameForSearch = search_field.getText().toLowerCase();
        storage_table.getItems().clear();
        storage_table.getItems().addAll(loadedFiles
                .stream()
                .filter(file -> file.getName().toLowerCase().matches(".*" + fileNameForSearch + ".*"))
                .collect(Collectors.toList()));
    }

    /**
     * <b>Выгружает записи из БД</b>
     *
     * @return список моделей {@link FileInStorage}
     */
    private ObservableList<FileInStorage> getTableData() {
        try {
            List<StorageResult> tableData = fileStorage.getAllFiles();
            ObservableList<FileInStorage> model = FXCollections.observableArrayList();
            for (int index = 0; index < tableData.size(); index++) {
                StorageResult item = tableData.get(index);
                model.add(new FileInStorage(
                                new SimpleIntegerProperty(index + 1),
                                new SimpleStringProperty(item.getCreateDate()),
                                new SimpleStringProperty(item.getName() + "." + item.getExpansion()),
                                new SimpleStringProperty(item.getGuid()),
                                new SimpleStringProperty(item.getExpansion()),
                                new SimpleBooleanProperty(item.isCreatedBefore())
                        )
                );
            }
            return model;
        } catch (DissertationException ex) {
            utils.showAlertWindow(Alert.AlertType.ERROR, "Ошибка получения файлов",
                    ex.getMessage());
        }
        return FXCollections.observableArrayList();
    }

    /**
     * <b>Переформировывает порядковые номера записей в таблице</b>
     */
    private void rewriteNumbers() {
        for (int i = 0; i < loadedFiles.size(); i++) {
            FileInStorage file = loadedFiles.get(i);
            file.setNumber(i + 1);
        }
    }
}
