package logvin.dissertation.forms.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logvin.dissertation.forms.controllers.EndScenario;
import logvin.dissertation.forms.controllers.FxmlLoaderModel;
import logvin.dissertation.utils.DissertationException;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

import static logvin.dissertation.utils.ExceptionModel.*;

/**
 * <b>Вспомогательный класс для работы с JavaFX формами</b>
 *
 * @author logvinIN
 * @since 23.06.2019
 */
public class ControllerUtils {

    /**
     * <b>Показывает новое окно для отображения системной информации или ошибок</b>
     *
     * @param alertType  тип сообщения {@link javafx.scene.control.Alert.AlertType}
     * @param headerText заголовок сообщения
     * @param message    сообщение
     */
    public void showAlertWindow(Alert.AlertType alertType, String headerText, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle("Информация");
        alert.setHeaderText(headerText);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * <b>Показывает окно для подтверждения операции пользователем</b>
     *
     * @param headerText заголовок сообщения
     * @param message    сообщение
     * @return признак того, что пользователь выбрал действие "ОК"
     */
    public boolean getConfirmResult(String headerText, String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Подтверждение");
        alert.setHeaderText(headerText);
        alert.setContentText(message);
        Optional<ButtonType> resultButton = alert.showAndWait();
        boolean result = false;
        if (resultButton.get() == ButtonType.OK) {
            result = true;
        }
        return result;
    }

    /**
     * <b>Показывает окно окончания сценария</b>
     *
     * @param resultFilePath путь к итоговому документу
     * @throws DissertationException если не удалось выполнить {@link FXMLLoader#load()}
     */
    public void showEndScenarioWindow(String resultFilePath) throws DissertationException {
        FXMLLoader loader = getLoaderWithSource("EndScenario.fxml");
        Parent loadedLoader;
        try {
            loadedLoader = loader.load();
            EndScenario controller = loader.getController();
            controller.setFilePathToOpen(resultFilePath);
            showForm("Сценарий завершен", loadedLoader);
        } catch (IOException ex) {
            throw new DissertationException(F_03);
        }
    }

    /**
     * <b>Открывает форму переданного {{@link FXMLLoader#load()}</b>
     *
     * @param formTitle    заголовок окна
     * @param loadedLoader {@link FXMLLoader#load()}
     * @return контейнер {@link Stage}
     */
    public Stage showForm(String formTitle, Parent loadedLoader) {
        Stage stage = new Stage();
        stage.setTitle(formTitle);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(loadedLoader));
        stage.show();
        return stage;
    }

    /**
     * <b>Получить {@link FXMLLoader#load()} по наименованию *.fxml</b>
     *
     * @param fxmlFileName наименование fxml-файла
     * @return {@link FXMLLoader}
     * @throws DissertationException если {@link Objects#requireNonNull(java.lang.Object)} для ресурса fxml вернул NPE
     */
    public FXMLLoader getLoaderWithSource(String fxmlFileName) throws DissertationException {
        try {
            return new FXMLLoader(Objects.requireNonNull(
                    getClass()
                            .getClassLoader()
                            .getResource("forms/fxml/" + fxmlFileName))
            );
        } catch (NullPointerException ex) {
            throw new DissertationException(F_01, fxmlFileName);
        }
    }

    /**
     * <b>Наполняет {@link FxmlLoaderModel} соответствующими значениями по переданному loader'у</b>
     *
     * @param loader лоадер формы
     * @return наполненный {@link FxmlLoaderModel}
     * @throws DissertationException если не удалось выполнить {@link FXMLLoader#load()}
     */
    public FxmlLoaderModel loadFxmlModel(FXMLLoader loader) throws DissertationException {
        try {
            return new FxmlLoaderModel(loader);
        } catch (IOException ex) {
            String[] filePath = loader.getLocation().getFile().split("/");
            throw new DissertationException(F_02, filePath[filePath.length - 1]);
        }
    }

    /**
     * <b>Устанавливает кнопкам признак невозможности использования</b>
     *
     * @param disableValue true, если надо "выключить" кнопки, false в ином случае
     * @param button       переменное количество кнопок
     * @param <T>          наследники JavaFX класса {@link ButtonBase}
     */
    @SafeVarargs
    public final <T extends ButtonBase> void setDisableOnButton(boolean disableValue, T... button) {
        for (T aButton : button) {
            aButton.setDisable(disableValue);
        }
    }
}
