package logvin.dissertation.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.jetbrains.annotations.Nullable;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.lang.Character.isAlphabetic;
import static java.lang.Character.isDigit;
import static org.apache.commons.lang3.ArrayUtils.removeElement;

/**
 * <b>Вспомогательный класс для работы с рабочей книгой</b>
 * //todo нужен рефакторинг в оставленных туду
 *
 * @author logvinIN
 * @since 28.10.2018
 */
public class ExcelUtils {

    /**
     * Массив английского алфавита для идентификации колонок в рабочей книге
     */
    private char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /**
     * Список колонок рабочей книги
     */
    private List<String> cellsNames = new ArrayList<>();

    /**
     * <b>Отделяет от адреса ячейки число, оставляя только буквы</b>
     *
     * @param cellAddress адрес ячейки
     * @return буквы адреса ячейки
     */
    private String cellIndexToFill(String cellAddress) {
        StringBuilder cell = new StringBuilder();
        if (StringUtils.isNotBlank(cellAddress)) {
            char[] indexChars = cellAddress.toCharArray();
            for (char character : indexChars) {
                if (isAlphabetic(character)) {
                    cell.append(String.valueOf(character));
                }
            }
            return cell.toString();
        } else {
            //todo что? подзабыл зачем эта ошибка
            throw new InvalidParameterException("Не установлен индекс колонки с СКО спроса");
        }
    }

    /**
     * <b>Формирует список колонок с помощью конкатенации значений одного и того же списка</b>
     *
     * @return список адресов колонок в формате "AA, AB, AC, ..., ZZ"
     */
    private List<String> getTwiceColumns() {
        List<String> columns = new ArrayList<>();
        for (char aChar : chars) {
            columns.add(String.valueOf(aChar));
        }
        for (char aChar : chars) {
            for (char aChar1 : chars) {
                columns.add(String.valueOf(aChar) + String.valueOf(aChar1));
            }
        }
        return columns;
    }

    /**
     * <b>Заполняет список {@link #cellsNames} адресами колонок с помощью конкатенации значений одного и того же списка</b>
     * <p>По итогу в списке будут тройные адреса колонок (AAA, AAB, AAC, AAD, ..., XFD)</p>
     * <p>На момент создания метода в рабочей книге Excel последняя колонка XFD</p>
     */
    public void fillColumnNames() {
        List<String> finalColumns = getTwiceColumns();
        for (char aChar : chars) {
            for (char aChar1 : chars) {
                for (char aChar2 : chars) {
                    if (!finalColumns.contains("XFD")) {
                        finalColumns.add(String.valueOf(aChar) + String.valueOf(aChar1 + String.valueOf(aChar2)));
                    }
                }
            }
        }
        //todo проверить, что тудуха ниже актуальна
        //todo доделать заполнение с А до XFD
        this.cellsNames = finalColumns;
    }

    /**
     * <b>Получить индекс колонки по адресу ячейки</b>
     *
     * @param cellAddress адрес ячейки
     * @return индекс колонки
     */
    public int getColumnIndexByAddress(String cellAddress) {
        String cellWithoutRow = cellIndexToFill(cellAddress);
        int cellIndex = cellsNames.indexOf(cellWithoutRow);
        if (cellIndex == -1) {
            throw new InvalidParameterException("");
        }
        return cellIndex;
    }

    /**
     * <b>Получить индекс строки по адресу ячейки</b>
     *
     * @param cellAddress адрес ячейки
     * @return индекс строки
     */
    public int getRowIndexByAddress(String cellAddress) {
        int toReturn = 0;
        if (StringUtils.isNotBlank(cellAddress)) {
            StringBuilder result = new StringBuilder();
            char[] cellAddressChars = cellAddress.toCharArray();
            for (Character aChar : cellAddressChars) {
                if (isAlphabetic(aChar)) {
                    cellAddressChars = removeElement(cellAddressChars, aChar);
                }
            }
            result.append(cellAddressChars);
            toReturn = Integer.parseInt(result.toString());
        }
        return toReturn - 1;
    }

    /**
     * <b>Динамическое получение значения из ячейки</b>
     *
     * @param cell ячейка
     * @return значение ячейки или пустая строка
     */
    public Object getValueFromCell(Cell cell) {
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case FORMULA:
                return cell.getCellFormula();
            case NUMERIC:
                return cell.getNumericCellValue();
            case ERROR:
                return cell.getErrorCellValue();
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case BLANK:
                return "";
            case _NONE:
                return "";
            default:
                return "";
        }
    }

    /**
     * <b>Проверяет, что значение ячейки можно сконвертировать в {@link Integer}</b>
     *
     * @param cell ячейка
     * @return признак того, что значение ячейки можно сконвертировать в {@link Integer}
     */
    public boolean isCellParsableToInt(Cell cell) {
        CellType type = cell.getCellType();
        return type == CellType.NUMERIC;
    }

    /**
     * <b>Преобразует значение ячейки из  {@link Double} в {@link Integer}</b>
     *
     * @param cellValue значение из ячейки с типом {@link CellType#NUMERIC}
     * @return преобразованное значение из ячейки с типом {@link CellType#NUMERIC}
     */
    @Nullable
    public Integer getIntFromDouble(Object cellValue) {
        if (cellValue instanceof Double) {
            return ((Double) cellValue).intValue();
        } else {
            return null;
        }
    }

    /**
     * <b>Идентифицирует и увеличивает индексы ячеек в формуле</b>
     *
     * @param formula формула, индексы ячеек в которой надо увеличить
     * @return формула с увеличенными индексами ячеек
     */
    public String increaseIndexes(String formula) {
        String copiedFormula = formula;
        formula = formula
                .replaceAll("\\s", "") //удалим пробелы
                .replaceAll("(\\$\\w\\$\\d)", "_") //уберем "полностью замороженные" ячейки
                //уберем "частично замороженные" ячейки, т.к. индексы у них не увеличить
                //todo доработать для увеличения букв
                .replaceAll("(\\w\\$\\d)", "_");

        char[] formulaChars = formula.toCharArray();
        List<Character> charList = new ArrayList<>();
        for (char f : formulaChars) {
            charList.add(f);
        }

        int freezeCellIdx = 0;
        //todo проверить актуальность данной операции
        for (int i = 0; i < charList.size(); i++) {
            if (isDigit(charList.get(i)) &&
                    Objects.equals('_', charList.get(i - 1))) {
                freezeCellIdx = i;
            }
        }

        for (int i = 0; i < charList.size(); i++) {
            int j = i;
            if (isDigit(charList.get(j)) &&
                    isMathOperator(charList.get(j - 1))) {
                charList.remove(j);
                i = j - 1;
            }
        }

        StringBuilder formula2 = new StringBuilder();

        for (char f : charList) {
            formula2.append(f);
        }

        String changedFormula = formula2.toString();
        List<String> incCells = new ArrayList<>();

        StringBuilder sb2;
        for (char c : formulaChars) {
            sb2 = new StringBuilder(changedFormula);
            if (isMathOperator(c)) {
                changedFormula = sb2.deleteCharAt(changedFormula.indexOf(c)).toString();
            }
        }
        formulaChars = changedFormula.toCharArray();
        charList.clear();
        for (char f : formulaChars) {
            charList.add(f);
        }

        for (int i = 0; i < charList.size(); i++) {
            int j = i;
            if (isMathOperator(charList.get(j))) {
                charList.remove(j);
                i = j - 1;
            }
        }


        charList = removeTrash(charList);

        StringBuilder afterRemove = new StringBuilder();
        for (char f : charList) {
            afterRemove.append(f);
        }

        List<String> cellsToInc = getCellsToIncrease(afterRemove.toString());

        for (String cell : cellsToInc) {
            incCells.add(increaseCellIndex(cell));
        }

        String formulaToOut = copiedFormula;
        for (int i = 0; i < cellsToInc.size(); i++) {
            //todo проверить актуальность отключения механизма
//            if (StringUtils.isNumeric(cellsToInc.get(i))) {
//                char char_ = incCells.get(i).charAt(0);
//                StringBuilder builder = new StringBuilder(formulaToOut);
//                builder.replace(freezeCellIdx - 1, freezeCellIdx, incCells.get(i));
//                formulaToOut = builder.toString();
//            } else {
            formulaToOut = formulaToOut.replace(cellsToInc.get(i), incCells.get(i));
        }
//        }
        return formulaToOut;

    }

    /**
     * <b>Проверяет, что символ является математическим оператором (неполный перчень)</b>
     *
     * @param c символ
     * @return признак того, что символ является математическим оператором
     */
    private boolean isMathOperator(char c) {
        return Objects.equals(c, '+') ||
                Objects.equals(c, '-') ||
                Objects.equals(c, '/') ||
                Objects.equals(c, '*') ||
                Objects.equals(c, '(') ||
                Objects.equals(c, ')') ||
                Objects.equals(c, '>') ||
                Objects.equals(c, '&') ||
                Objects.equals(c, ':') ||
                Objects.equals(c, '=') ||
                Objects.equals(c, '<') ||
                Objects.equals(c, '"') ||
                Objects.equals(c, ';') ||
                Objects.equals(c, '.') ||
                Objects.equals(c, '!') ||
                Objects.equals(c, ',');
    }

    /**
     * <b>Убирает "мусор" из списка символов формулы</b>
     *
     * @param charList список символов
     * @return список символов формулы без "мусора"
     */
    private List<Character> removeTrash(List<Character> charList) {
        for (int i = 0; i < charList.size(); i++) {
            int j = i;
            int sizeAfterRemove = charList.size();
            if (j != sizeAfterRemove - 1) {
                //todo доработать!!!
                //todo сейчас удаляет букву, если справа есть буква
                //не работает для двойных и тройных ячеек
                //если будет ABC5, то после запуска алгоритма останется C5
                if (isAlphabetic(charList.get(j)) &&
                        (isAlphabetic(charList.get(j + 1)) || charList.get(j + 1) == '_')) {
                    charList.remove(j);
                    i = j - 1;
                }
            } else if (isAlphabetic(charList.get(j))) {
                charList.remove(j);
                i = j - 1;
            }
        }
        return charList;
    }


    /**
     * <b>Увеличивает индекс ячейки</b>
     *
     * @param cell адрес ячейки
     * @return увеличенный на единицу адрес ячейки
     */
    private String increaseCellIndex(String cell) {
        char[] array = cell.toCharArray();
        StringBuilder removedChar = new StringBuilder();
        for (char c : array) {
            if (isAlphabetic(c)) {
                removedChar.append(String.valueOf(c));
                array = removeElement(array, c).clone();
            }
        }
        int value = Integer.parseInt(String.copyValueOf(array)) + 1;

        return removedChar.toString() + value;
    }

    /**
     * <b>Увеличивает адрес колонки</b>
     *
     * @param cell адрес колонки
     * @return увеличенный на следующую букву адрес колонки
     */
    private String increaseColumnName(String cell) {
        char[] array = cell.toCharArray();
        StringBuilder removedChar = new StringBuilder();
        for (char c : array) {
            if (isDigit(c)) {
                removedChar.append(String.valueOf(c));
                array = removeElement(array, c).clone();
            }
        }
        String value = String.copyValueOf(array);
        String nextValue = this.cellsNames.get(this.cellsNames.indexOf(value) + 1);
        return nextValue + removedChar.toString();
    }

    /**
     * <b>Идентифицирует в ячейки в строке</b>
     * <p>(например C5C6C7C8, по итогу алгоритм вставит делимитеры C5_C6_C7_C8</p>
     *
     * @param preparedFormula формула с удаленным мусором
     * @return список увеличенных адресов ячеек
     */
    private List<String> getCellsToIncrease(String preparedFormula) {
        char[] cellChars = preparedFormula.toCharArray();
        List<Character> charList = new ArrayList<>();
        for (char f : cellChars) {
            charList.add(f);
        }
        List<Character> changedCharList = new ArrayList<>(charList);
        List<String> cells;
        char delimiter = '_';
        for (int i = 0; i < charList.size(); i++) {
            if (i != 0 && isAlphabetic(charList.get(i))) {
                changedCharList.add(i, delimiter);
                ++i;
                charList = changedCharList;
                //todo проверить актуальность и убрать
//                preparedFormula = new StringBuilder(preparedFormula).insert(preparedFormula.indexOf(c), delimiter).toString();
            }
        }
        StringBuilder increased = new StringBuilder();
        for (char f : changedCharList) {
            increased.append(f);
        }
        cells = Arrays.stream(increased.toString()
                .split("[_$]"))
                .filter(StringUtils::isNoneBlank)
                .collect(Collectors.toList());
        return cells;
    }


    /**
     * <b>Разбивает диапазон ячеек на список ячеек этого диапазона</b>
     * <p>Работает только вниз, т.е. увеличивает цифры</p>
     *
     * @param range диапазон ячеек
     * @return например передали A1:A5. Метод вернет A1, A2, A3, A4, A5
     */
    public List<String> parseRangeToCellsDown(String range) {
        List<String> rangeCells = new ArrayList<>();
        String[] rangeOfRange = range.split(";");
        for (String r : rangeOfRange) {
            String startCell = r.split(":")[0];
            if (r.split(":").length > 1) {
                String endCell = r.split(":")[1];
                rangeCells.add(startCell);
                String cellInRange = startCell;
                //todo сейчас работает только вниз, надо еще блочное увеличение диапазона (например A1:D5)
                while (!Objects.equals(cellInRange, endCell)) {
                    cellInRange = increaseCellIndex(cellInRange);
                    rangeCells.add(cellInRange);
                }
            } else {
                rangeCells.add(startCell);
            }
        }
        return rangeCells;
    }

    /**
     * <b>Разбивает диапазон ячеек на список ячеек этого диапазона</b>
     * <p>Работает только вправо, т.е. увеличивает буквы</p>
     *
     * @param range диапазон ячеек
     * @return например передали A1:D1. Метод вернет A1, B1, C1, D1
     */
    public List<String> parseRangeToCellsRight(String range) {
        this.fillColumnNames();
        String startCell = range.split(":")[0];
        List<String> rangeCells = new ArrayList<>();
        if (range.split(":").length > 1) {
            String endCell = range.split(":")[1];
            rangeCells.add(startCell);
            String cellInRange = startCell;
            while (!Objects.equals(cellInRange, endCell)) {
                cellInRange = increaseColumnName(cellInRange);
                rangeCells.add(cellInRange);
            }
        } else {
            rangeCells.add(startCell);
        }
        return rangeCells;
    }


    public ParseDirection getRangeParseDirection(String range) {
        this.fillColumnNames(); //todo переделать так, чтобы вызывалось один раз
        char[] startCell = range.split(":")[0].toCharArray();
        char[] endCell = range.split(":")[1].toCharArray();
        StringBuilder startColumnIndex = new StringBuilder();
        StringBuilder endColumnIndex = new StringBuilder();

        for (int i = 1; i <= startCell.length; i++) {
            char startCellSymbol = startCell[startCell.length - i];
            if (isDigit(startCellSymbol)) {
                startColumnIndex.append(startCellSymbol);
            }else{
                break; //при первой попавшейся не цифре прерываем цикл
            }
        }
        for (int i = 1; i <= endCell.length; i++) {
            char endCellSymbol = endCell[endCell.length - i];
            if (isDigit(endCellSymbol)) {
                endColumnIndex.append(endCellSymbol);
            } else{
                break; //при первой попавшейся не цифре прерываем цикл
            }
        }
        startColumnIndex.reverse();
        endColumnIndex.reverse();
        return Objects.equals(startColumnIndex.toString(), endColumnIndex.toString()) ?
                ParseDirection.RIGHT :
                ParseDirection.DOWN;
    }


    public String getValueFromFunction(Supplier<String> lambdaConsumer){
        return lambdaConsumer.get();
    }


    public <Key, Value> Map<Key, Value> getHashMap(Key key, Value value) {
        Map<Key, Value> hashMap = new HashMap<>();
        hashMap.put(key, value);
        return hashMap;
    }
}
